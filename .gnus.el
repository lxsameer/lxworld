;;; gnus --- Gnus configuration
;;; Commentary:
;;; Code:
;; (depends-on 'nnhackernews)
;; (depends-on 'nnreddit)

(require 'gnus-agent)
(require 'gnus-async)
(require 'gnus-group)
(require 'gnus-srvr)

(setq auth-sources '((:source "~/.authinfo.gpg")))

(setq user-mail-address  "lxsameer@gnu.org"
      user-full-name     "Sameer Rahmani")

(setq mail-user-agent 'message-user-agent)
(setq message-mail-user-agent nil)
(setq message-kill-buffer-on-exit t)
(setq message-wide-reply-confirm-recipients t)

(setq message-citation-line-format "%f [%Y-%m-%d, %R %z]:\n")
(setq message-citation-line-function
      'message-insert-formatted-citation-line)
(add-to-list 'mm-body-charset-encoding-alist '(utf-8 . base64))
(setq gnus-check-new-newsgroups 'ask-server)
(setq gnus-read-active-file 'some)


(setq gnus-agent-article-alist-save-format 1)  ; uncompressed
(setq gnus-agent-cache t)
(setq gnus-agent-confirmation-function 'y-or-n-p)
(setq gnus-agent-consider-all-articles nil)
(setq gnus-agent-directory "~/news/agent/")
(setq gnus-agent-enable-expiration 'ENABLE)
(setq gnus-agent-expire-all nil)
(setq gnus-agent-expire-days 30)
(setq gnus-agent-mark-unread-after-downloaded t)
(setq gnus-agent-queue-mail t)        ; queue if unplugged
(setq gnus-agent-synchronize-flags nil)
(setq gnus-article-browse-delete-temp 'ask)
(setq gnus-article-over-scroll nil)
(setq gnus-article-show-cursor t)
(setq gnus-article-sort-functions
      '((not gnus-article-sort-by-number)
        (not gnus-article-sort-by-date)))
(setq gnus-article-truncate-lines nil)
(setq gnus-inhibit-images t)
(setq gnus-treat-display-smileys nil)
(setq gnus-article-mode-line-format "%G %S %m")
(setq gnus-visible-headers
        '("^From:" "^To:" "^Cc:" "^Newsgroups:" "^Subject:" "^Date:"
          "Followup-To:" "Reply-To:" "^Organization:" "^X-Newsreader:"
          "^X-Mailer:"))

(setq gnus-asynchronous t)
(setq gnus-use-article-prefetch 15)
(setq gnus-level-subscribed 6)
(setq gnus-level-unsubscribed 7)
(setq gnus-level-zombie 8)
(setq gnus-list-groups-with-ticked-articles nil)
(setq gnus-group-sort-function
      '((gnus-group-sort-by-unread)
        (gnus-group-sort-by-alphabet)
        (gnus-group-sort-by-rank)))
(setq gnus-group-mode-line-format "%%b")
(setq gnus-auto-select-first nil)
(setq gnus-summary-ignore-duplicates t)
(setq gnus-suppress-duplicates t)
(setq gnus-summary-goto-unread nil)
(setq gnus-summary-make-false-root 'adopt)
(setq gnus-summary-thread-gathering-function
      'gnus-gather-threads-by-subject)
(setq gnus-thread-sort-functions
      '((not gnus-thread-sort-by-date)
        (not gnus-thread-sort-by-number)))
(setq gnus-subthread-sort-functions
      'gnus-thread-sort-by-date)
(setq gnus-thread-hide-subtree nil)
(setq gnus-thread-ignore-subject nil)

(setq gnus-user-date-format-alist
      '(((gnus-seconds-today) . "Today at %R")
        ((+ 86400 (gnus-seconds-today)) . "Yesterday, %R")
        (t . "%Y-%m-%d %R")))
(setq gnus-summary-line-format "%U%R%z %-16,16&user-date;  %4L:%-30,30f  %B%S\n")
(setq gnus-summary-mode-line-format "%p")
(setq gnus-sum-thread-tree-false-root "─┬➤ ")
(setq gnus-sum-thread-tree-indent " ")
(setq gnus-sum-thread-tree-leaf-with-other "├─➤ ")
(setq gnus-sum-thread-tree-root "")
(setq gnus-sum-thread-tree-single-leaf "└─➤ ")
(setq gnus-sum-thread-tree-vertical "│")

(add-hook 'gnus-select-group-hook 'gnus-group-set-timestamp)
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)
(add-hook 'gnus-summary-exit-hook 'gnus-topic-sort-groups-by-alphabet)
(add-hook 'gnus-summary-exit-hook 'gnus-group-sort-groups-by-rank)

(setq gnus-select-method '(nnnil ""))

(setq mail-sources
      '((pop :server "fencepost.gnu.org"
             :port 995
             :user "lxsameer"
             :authentication 'apop
             :stream ssl)))

(setq gnus-secondary-select-methods
      '((nntp "news.gwene.org")
        ;;(nnhackernews "")
        ;;(nnreddit "")
        (nnml "")))

(setq gnus-use-cache t)

; Use fancy splitting:
(setq nnmail-split-methods 'nnmail-split-fancy)

; Email splitting rules:
(setq nnmail-split-fancy
      '(|
        ("from" ".*@udemy\\.com" "udemy")
        ("subject" ".*udemy\\/.*" "udemy-prs")
        (any "linux-doc@vger\\.kernel\\.org" "kernel-docs")
        (any "emacs-devel@gnu\\.org" "emacs-devel")
        "inbox"))
(setq gnus-group-line-format "%M%S%p%P%5y:%B%(%g%)
")
;;(gnus-topic-create-topic "Kernel" nil "Gnus")
(defun my-gnus-group-list-subscribed-groups ()
  "List all subscribed groups with or without un-read messages"
  (interactive)
  (gnus-group-list-all-groups 5))

(define-key gnus-group-mode-map
  ;; list all the subscribed groups even they contain zero un-read messages
  (kbd "o") 'my-gnus-group-list-subscribed-groups)

(setq gnus-topic-topology
      '(("Gnus" visible)
        (("Mails" visible))
        (("Kernel" visible))))

;; (("Gnus" visible) (("misc" visible)) (("Kernel" visible nil nil)))
(setq send-mail-function
      'smtpmail-send-it
      smtpmail-smtp-server  "fencepost.gnu.org"
      smtpmail-stream-type  'starttls
      smtpmail-smtp-service 587)
