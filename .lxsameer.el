;;; lxsameer.el --- My personal emacs configuration
;;; Commentary:
;;; Code:
(require 'server)

(add-to-list 'exec-path
             "/home/lxsameer/bin")

(add-to-list 'default-frame-alist '(font . "xos4 terminus-12"))
;;(set-face-attribute 'default t :font "xos4 terminus")
(set-frame-font "xos4 terminus-12")

(when-wm
 (add-to-list 'default-frame-alist '(font . "xos4 terminus-10"))
 (set-frame-font "xos4 terminus-10"))


(defun pinentry-setup ()
  "Setup the pinentry."
  (interactive)
  (depends-on 'pinentry)
  (require 'pinentry)
  (setq epa-pinentry-mode 'loopback)
  (pinentry-start))

(global-set-key (kbd "<f7>") 'imenu-list-smart-toggle)
(global-set-key (kbd "<f6>") 'org-capture)


(setq org-directory "~/orgs/")
(setq org-default-notes-file "~/orgs/notes.org")


(setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/orgs/main.org" "New Tasks")
         (file "~/orgs/templates/todo")
         :prepend t)
        ("l" "Link" entry (file+headline "~/orgs/bookmarks.org" "Links")
         (file "~/orgs/templates/links")
         :prepend t
         :empty-lines 1)
        ("h" "Thoughts" entry (file+datetree "~/orgs/journal.org")
         (file "~/orgs/templates/thoughts"))))

(setq org-refile-targets '((org-agenda-files :maxlevel . 3)))

(with-eval-after-load "ox-latex"
  (setq org-latex-default-packages-alist
	(append '(("hidelinks" "hyperref" nil)) org-latex-default-packages-alist)))


(setq gdscript-godot-executable "~/bin/godot")

(defun lsp--gdscript-ignore-errors (original-function &rest args)
  "Ignore the error message resulting from Godot not replying to the `JSONRPC' request."
  (if (string-equal major-mode "gdscript-mode")
      (let ((json-data (nth 0 args)))
        (if (and (string= (gethash "jsonrpc" json-data "") "2.0")
                 (not (gethash "id" json-data nil))
                 (not (gethash "method" json-data nil)))
            nil ; (message "Method not found")
          (apply original-function args)))
    (apply original-function args)))
;; Runs the function `lsp--gdscript-ignore-errors` around `lsp--get-message-type` to suppress unknown notification errors.
(advice-add #'lsp--get-message-type :around #'lsp--gdscript-ignore-errors)

(unless (server-running-p)
  (server-start)
  (require 'org-protocol))



;; (defun lx/msg (p)
;;   (org-capture nil "l")
;;   (message "<<<<<<< %s" p)
;;   nil)

;; (setq org-protocol-protocol-alist
;;       (list
;;        (list "msg" :protocol "msg" :function #'lx/msg)))
(setq lsp-clients-clangd-executable "~/usr/lib/llvm-13/bin/clangd")


(defconst LLVM_DIR "/home/lxsameer/src/serene/llvm-project")
(defconst llvm-config-dir (concat LLVM_DIR "/llvm/utils/emacs/"))

(depends-on 'company-box)

(load (concat llvm-config-dir "emacs.el"))
(load (concat llvm-config-dir "llvm-mode.el"))
(load (concat llvm-config-dir "tablegen-mode.el"))

(add-hook 'c++-mode-hook (lambda ()
			   (lsp)
			   (require 'company-capf)
                           (require 'company-box)
                           (add-hook 'company-mode-hook 'company-box-mode)))

(setq company-backends
      '((company-capf
         company-yasnippet
         company-keywords)))


(add-hook 'before-save-hook
          (lambda ()
            (if (bound-and-true-p lsp-mode)
                (lsp-format-buffer))))


(provide '.lxsameer)
;;; .lxsameer.el ends here
