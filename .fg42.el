;; Uncomment this for debugging purposes
(setq debug-on-error t)
;; THEME
;; =====
;; Load the default theme
;; Other options are:
;; (theme themes/color-theme-spacemacs-light)
;; (theme themes/color-theme-spacemacs-dark)
;; (theme themes/color-theme-doom-dracula)
(require 'fg42/wm)
(initialize-wm)

(defability wm-randr ()
  "RandR support for WM"
  (when-wm
   (require 'exwm-randr)
   (setq exwm-randr-workspace-output-plist '(0 "HDMI-1"
                                               1 "HDMI-1"
                                               2 "HDMI-1"
                                               3 "HDMI-1"
                                               4 "HDMI-1"
                                               5 "HDMI-1"
                                               6 "eDP-1"
                                               7 "HDMI-1"
                                               8 "HDMI-1"
                                               9 "HDMI-1"))
   (add-hook 'exwm-randr-screen-change-hook
             (lambda ()
               (start-process-shell-command
                "xrandr" nil "xrandr --output HDMI-1 --above eDP-1 --mode 1920x1080")))
   (exwm-randr-enable)))


(theme themes/color-theme-doom-one)
;; (theme themes/color-theme-doom-molokai)

;; ABILITIES
;; =========
;; Disable abilities which you don't want.
(disable 'rbenv 'helm 'spell 'linum 'tabbar 'smex 'swiper 'ivy
         'smart-mode-line 'desktop-mode 'jedi
         'dired+ 'guru 'emoji 'elpy 'github
         'versioned-backup)


;; EXTENSIONS
;; ==========
;; Uncomment extensions that you may need.
(activate-extensions 'editor
                     'development
                     ;;'web
                     'editor-theme
                     'javascript
                     ;;'ruby
                     'cpp
                     'clojure
                     ;;'haskell
                     ;;'php
                     ;;'common-lisp
                     'python
                     'serene
                     'rust
                     'typescript
                     ;;'arduino
                     'java)
                     ;;'racket
                     ;;'irc
                     ;;'latex)


;; What are you ?
(i-am-god)

;; i-am-god   => Emacs user.
;; i-am-human => A user of other boring editors.
;; i-am-evil  => An evil user. A vim user.


;; Example of things you can do in your ~/.fg42.el config file:
;;
;; Setting your TODO file path:
;;   (setq fg42-todo-file "~/.TODO.org")
;; or you can open a remote TODO file
;;   (add-hook 'fg42-before-open-todo-hook 'disable-projectile)
;;   (setq fg42-todo-file "/ssh:user@host:/home/USER/.TODO.org")
;;
;; Set some environment variables for your fg42 to use
;;  (setenv "http_proxy" "localhost:8118")
;;  (setenv "https_proxy" "localhost:8118")
;;
;; Alwasy open a your TODO file as your first buffer
;;  (add-hook 'fg42-after-initialize-hook 'fg42-open-todo)

;; If you're using tools like rbenv or nodenv or other similar tools
;; to manage versions of your favorite language, then you need to
;; add their shims to your path. Follow the example below:
;;
;; (setenv "PATH" (concat "/home/USER/.rbenv/shims:"
;;                        "/home/USER/.nodenv/shims:"
;;                        "/home/USER/bin:" (getenv "PATH")))
;;
;; Ofcourse you need to change the USER to your username

;; If you have a separate bin directory as well you can add it
;; to the exec-path as follows:
;;
(add-to-list 'exec-path "/home/lxsameer/bin")
;;
;; abilities like clojure which uses the lien tools need to find
;; specific tools (lein in this case) in your exec-path.


;; IRC Extension configuration ------------------------------------------------
;; Setup all the servers and channels you need
;; (setq irc-servers
;;   '(("irc.freenode.net"
;;      :port 6697
;;      :encryption tls
;;      :channels ("#5hit" "#emacs"))))

;; Set you nickname
;; (setq irc-nickname "awesome-fg42-user")

;; You can use password manager for storing you IRC credentials.
;; NOTE: you need to have a pair of GPG keys for this.
;;
;; Add the following to your ~/.authinfo.gpg :
;;
;; machine freenode login <username> port nickserv password <password>
;;
;; Or if you don't want to use the password manager your can directly
;; set it up like this:
;; (setq irc-auth '(("freenode" nickserv "some_user" "p455w0rd")
;;                  ("freenode" chanserv "some_user" "#channel" "passwd99"))


(add-to-list 'exec-path "/home/lxsameer/.nodenv/shims/")
(add-to-list 'exec-path "/home/lxsameer/.pyenv/shims/")

(setq pt-executable "~/bin/pt")
(depends-on 'sml-mode)

(defun find-file-in-region ()
  (interactive)
  (let* ((text (buffer-substring-no-properties (region-beginning) (region-end)))
         (path-info (split-string text ":"))
         (path (car path-info))
         (linnum (cadr path-info)))
    (message ">>>" path-info)
    (find-file path)
    (when linnum
      (forward-line (- (string-to-number linnum) 1)))))


(defun firefox ()
  "Execute firefox."
  (interactive)
  (run-program "~/bin/firefox/firefox"))

(defun gscreenshot ()
  "Execute firefox."
  (interactive)
  (run-program "gnome-screenshot"))

(global-set-key (kbd "C-c <print>") 'gscreenshot)


(defun weechat ()
  "Execute weechat."
  (interactive)
  (run-program-in-xterm "weechat"))


(defun htop ()
  "Execute htop."
  (interactive)
  (run-program-in-xterm "htop"))


;; (defpreload fg42-elisp
;;   "eLisp extension for FG42"
;;   :version "3.0.0"
;;   :file-suffixes '("\\.el\\'"))

(defun prettier ()
  (interactive)
  (setq prettier-js-command "/home/lxsameer/src/Codamic/madara/client/node_modules/.bin/prettier")
  (require 'prettier-js)
  (prettier-js-mode t))


(defun c++-format ()
  (load clang-format-path))


(depends-on 'prettier-js)
(add-hook 'js2-mode-hook #'prettier)
(add-hook 'typescript-mode-hook 'prettier)
(add-hook 'before-save-hook
          (lambda ()
            (when (eq major-mode "typescript-mode")
              (prettier-js))))
(add-hook 'web-mode-hook 'prettier-js-mode)

(setq lsp-clients-clangd-executable "clangd-11")

(require 'fg42/brain-switch)

;;; .fg42.el ends here
