#! /bin/sh
xset b off
dunst &
seq 0 1 | xargs -l1 -I@ compton -b -d :0.@
setxkbmap -rules evdev -model pc105 -layout "us,ir" -option "grp:shifts_toggle"
xinput set-prop 12 'libinput Accel Speed' 1
xrdb -merge ~/.Xdefaults
xrdb -merge ~/.Xresources
xmodmap ~/.xmodmap
tmux start-server
eval $(ssh-agent)
