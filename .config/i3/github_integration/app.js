const Octokit = require("@octokit/rest");
const fs = require("fs");

const client = new Octokit({
  // see "Authentication" section below
  auth: `token ${process.env.GITHUB_TOKEN}`,

  // setting a user agent is required: https://developer.github.com/v3/#user-agent-required
  // v1.2.3 will be current @octokit/rest version
  userAgent: "octokit/rest.js v1.2.3",

  // add list of previews you’d like to enable globally,
  // see https://developer.github.com/v3/previews/.
  // Example: ['jean-grey-preview', 'symmetra-preview']
  previews: [],

  // set custom URL for on-premise GitHub Enterprise installations
  baseUrl: "https://api.github.com",

  // pass custom methods for debug, info, warn and error
  log: {
    debug: () => {},
    info: () => {},
    warn: console.warn,
    error: console.error
  },

  request: {
    // Node.js only: advanced request options can be passed as http(s) agent,
    // such as custom SSL certificate or proxy settings.
    // See https://nodejs.org/api/http.html#http_class_http_agent
    agent: undefined,

    // request timeout in ms. 0 means no timeout
    timeout: 0
  }
});

const reasons = [
  "mention",
  "assign",
  "author",
  "comment",
  "review_requested",
  "team_mention"
];

function processNotifications(result) {
  return result.data
    .filter(x => reasons.indexOf(x.reason) != -1)
    .reduce((acc, x) => {
      acc[x.reason] = acc[x.reason] || [];
      acc[x.reason].push(x);
      return acc;
    }, {});
}

function checkGithub() {
  const owner = "udemy";
  const repo = "website-django";
  const participating = true;
  const all = false;

  return client.activity
    .listNotificationsForRepo({
      owner,
      repo,
      all,
      participating
    })
    .then(data => {
      const result = processNotifications(data);
      return Object.keys(result).reduce((acc, x) => {
        acc[x] = result[x] ? result[x].length : 0;
        return acc;
      }, {});
    })
    .then(result => {
      return reasons.reduce((acc, x) => {
        acc[x] = acc[x] || 0;
        return acc;
      }, result);
    })
    .then(result =>
      Object.keys(result)
        .map(x => [x, result[x]])
        .sort()
    );
}

setInterval(() => {
  checkGithub().then(out => {
    const values = out.map(x => x[1]);
    let data = reasons.sort().join(",");
    data = data + "\n" + values.join(",");

    fs.writeFile(`${process.env.HOME}/.github_status`, data, err => {
      if (err) {
        console.error("Error:", err);
      }
    });
  });
}, 60000);
