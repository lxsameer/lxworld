#! /bin/bash

set -e

command=$1

case $command in
    start)
        GITHUB_TOKEN=59af9e028efb1e9d40977fd56b9d221a2077836a node $HOME/src/lxworld/i3/github_integration/app.js
        ;;

    mention)
        mentions=$(cat $HOME/.github_status|tail -n 1|cut -d , -f 4)
        echo "Mentions: ${mentions}"
        echo "Mentions: ${mentions}"
        if (($mentions > 0)); then
            echo "#22ee00"
        fi
        ;;
    team)
        mentions=$(cat $HOME/.github_status|tail -n 1|cut -d , -f 6)
        echo "Team: ${mentions}"
        echo "Team: ${mentions}"
        if (($mentions > 0)); then
            echo "#ffffff"
        fi
        ;;
    pr)
        mentions=$(cat $HOME/.github_status|tail -n 1|cut -d , -f 5)
        echo "Review Requested: ${mentions}"
        echo "RR: ${mentions}"
        if (($mentions > 0)); then
            echo "#22ee00"
        fi
        ;;
    pid)
        pid=$(ps aux | grep 'status/app.js' | awk '{print $2}' 2> /dev/null)
        if [ -z "$pid" ]; then
            echo "NO SERVER"
            echo "NO SERVER"
            echo "#ee2222"
        else
            echo "PID: ${pid}"
            echo "PID: ${pid}"
        fi
        ;;
    assign)
        mentions=$(cat $HOME/.github_status|tail -n 1|cut -d , -f 3)
        echo "Comment: ${mentions}"
        echo "Comment: ${mentions}"
        if (($mentions > 0)); then
            echo "#ffffff"
        fi
        ;;
    assign)
        mentions=$(cat $HOME/.github_status|tail -n 1|cut -d , -f 1)
        echo "Assign: ${mentions}"
        echo "Assign: ${mentions}"
        if (($mentions > 0)); then
            echo "#ffffff"
        fi
        ;;
    author)
        mentions=$(cat $HOME/.github_status|tail -n 1|cut -d , -f 2)
        echo "Author: ${mentions}"
        echo "Author: ${mentions}"
        if (($mentions > 0)); then
            echo "#22ee00"
        fi
        ;;
    *)
        echo "What ?"
        ;;
esac
